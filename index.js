(async function() {
    var mock = await import('./MOCK_DATA.json');
    
    var content = document.getElementById('content');
    
    var result = mock.reduce( (acc, item) => acc + 
        `<div class="card">
            <h3>${item.first_name} ${item.last_name}</h3>
            <div class="image"><img src="${item.photo}"></div>
            <div class="infos">${item.email}</div>
        </div>`,
    ''); 
    content.innerHTML = result;

})();
