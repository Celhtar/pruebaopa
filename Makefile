.PHONY: dev build clean help node_clean
.DEFAULT_GOAL := help 

##@ Help
help: ## Shows this help.
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[36m<target>\033[0m\n"} /^[a-zA-Z_-]+:.*?##/ { printf "  \033[36m%-15s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' $(MAKEFILE_LIST)

##@ Dev
dev: node_modules ## Dev server at http://localhost:1234 
	@yarn dev --open

build: clean node_modules ## Builds production artefact
	@yarn build --no-source-maps

clean: ## Removes artefacts
	@rm -rf dist 

node_clean: ## Removes node_modules
	@rm -rf node_modules 

reset: clean node_clean ## Only keep the source
	@rm -rf .cache

node_modules:
	@yarn
